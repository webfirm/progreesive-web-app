import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { Coffee } from '../logic/coffee';
import { GeoLocationService } from '../services/geo-location.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  public list: [Coffee];

  cof = new Coffee();

  constructor(private data: DataService, private map: GeoLocationService, private route: Router) { }

  ngOnInit() {
    this.data.getList(list => {
      this.list = list;
    });
    console.log('this.list', this.list[0].location);
  }

  getMap(coffee: Coffee) {
    console.log(coffee.location)
    const mapURL = this.map.getMapLink(coffee.location);
    window.open(mapURL);
  }

  moreInfo(coffee: Coffee) {
    this.route.navigate([`/coffee/${coffee.id}`]);
  }

  share(coffee: Coffee) {
    console.log(coffee);
    const text = `Hey I'm having ${coffee.name} at ${coffee.place}. Wanna join?`;
    if (navigator['share']) {
      console.log("inside share");
      navigator['share']({
        title: coffee.name,
        text: text,
        url: window.location.href
      }).then(() => console.log("sharrrred")).catch(error => console.log("errror:", error));
    } else {
      console.log('whatsssaaap');
      location.href = `whatsapp://send?text=${encodeURIComponent(text)}`;
    }
  }

}
