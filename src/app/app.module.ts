import { Routes, RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { GeoLocationService } from './services/geo-location.service';
import { DataService } from './services/data.service';
import { ServiceWorkerModule } from '@angular/service-worker'
import { environment } from '../environments/environment';

import {
  MatButtonModule, MatIconModule, MatInputModule, MatSelectModule, MatSliderModule, MatToolbarModule,
  MatCardModule, MatSlideToggleModule, MatProgressSpinnerModule, MatSnackBarModule
} from '@angular/material';

import 'hammerjs';
import { ListComponent } from './list/list.component';
import { CoffeeComponent } from './coffee/coffee.component';

import { FormsModule } from '@angular/forms';

const routes: Routes = [
  { path: '', component: ListComponent },
  { path: 'coffee', component: CoffeeComponent },
  { path: 'coffee/:id', component: CoffeeComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    CoffeeComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule, MatIconModule, MatInputModule, MatSelectModule, MatSliderModule, MatToolbarModule,
    MatCardModule, MatSlideToggleModule, MatProgressSpinnerModule, MatSnackBarModule,
    FormsModule,
    environment.production ? ServiceWorkerModule.register('/ngsw-worker.js') : []
  ],
  providers: [GeoLocationService, DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
