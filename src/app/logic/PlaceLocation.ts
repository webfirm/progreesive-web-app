export class PlaceLocation {

  constructor(public address: string = '',
    public city: string = '',
    public latitude: number = null,
    public longitude: number = null) {
      console.log(this.address)
  }
}
