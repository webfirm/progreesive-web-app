import { PlaceLocation } from './PlaceLocation';
import { TasteRating } from './TasteRating';
export class Coffee {
  type: string;
  rating: number;
  notes: string;
  tasteRating: TasteRating;

  // tslint:disable-next-line:max-line-length
  constructor(public id: number = null, public name: string = '', public place: string = '', public location: PlaceLocation = null) {
    // this.location = new PlaceLocation();
    // console.log(this.location)
    this.tasteRating = new TasteRating();
  }
}

