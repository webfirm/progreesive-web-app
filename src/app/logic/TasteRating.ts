export class TasteRating {
  aroma: number;
  body: number;
  flavor: number;
  intensity: number;
  sweeteness: number;
  afterTaste: number;
}
