import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Coffee } from '../logic/coffee';
import { GeoLocationService } from '../services/geo-location.service';
import { DataService } from '../services/data.service';
import { PlaceLocation } from '../logic/PlaceLocation';


@Component({
  selector: 'app-coffee',
  templateUrl: './coffee.component.html',
  styleUrls: ['./coffee.component.scss']
})
export class CoffeeComponent implements OnInit, OnDestroy {
  public routerSubscription: any;
  public coffee: Coffee;
  public coffeeInfo: any;
  showError = false;
  showSpinner = false;
  id = 0;
  isParam = false;
  paramId: any;
  public coffeeTypes: any[] = [
    'CoffeType1',
    'CoffeType2',
    'CoffeType3',
    'CoffeType4',
    'CoffeType5',
  ];
  constructor(private route: ActivatedRoute, private router: Router, private geoLocation: GeoLocationService, private data: DataService) { }

  ngOnInit() {
    this.coffee = new Coffee();
    this.coffee.location = new PlaceLocation();
    this.routerSubscription = this.route.params.subscribe(params => {
      if (params['id'] > 0) {
        this.isParam = true;
        this.paramId = params['id'];
        this.showCoffee();
      } else {
        this.isParam = false;
      }
    });

    this.geoLocation.requestLocation(location => {
      console.log(location);
      if (location) {
        this.coffee.location.latitude = location.latitude;
        this.coffee.location.longitude = location.longitude;
      }
    });

    console.log('this.coffee', this.coffee.location);
  }

  ngOnDestroy() {
    this.routerSubscription.unsubscribe();
  }

  addCoffee() {
    this.data.getList(list => {
      this.id = list.length;
    });
    // tslint:disable-next-line:max-line-length
    if ((this.coffee.name !== '') && (this.coffee.notes !== '') && (this.coffee.location) && (this.coffee.place !== '') && (this.coffee.type !== '')) {
      this.showSpinner = true;
      this.coffee.id = this.id + 1;
      this.data.addCoffee(this.coffee);
      setTimeout(() => {
        this.router.navigate(['/']);
      }, 1000);
    } else {
      this.showError = true;
    }
  }

  navigateToHome() {
    this.showSpinner = true;
    setTimeout(() => {
      this.router.navigate(['/']);
    }, 1000);
  }

  showCoffee() {
    if (this.paramId) {
      this.data.getList(list => {
        console.log(list[0].id);
        this.coffeeInfo = list.filter(coffee => coffee.id == this.paramId);
        this.coffee = this.coffeeInfo[0];
      });
    }
    console.log("this.coffee",this.coffee)
  }

}
