import { Injectable } from '@angular/core';
import { Coffee } from '../logic/coffee';
import { PlaceLocation } from '../logic/PlaceLocation';

@Injectable()
export class DataService {
  public list = [
    new Coffee(1, 'Espresso', 'Petlad Cafe', new PlaceLocation('Sardar Patel Mall', 'Petlad', 23.033179, 72.560004)),
    new Coffee(2, 'Hot Chocolate', 'Some Other Cafe', new PlaceLocation('Some other Mall', 'Anand', 22.549910, 72.931006))
  ];
  constructor() { }

  getList(callback) {
    console.log(this.list);
    callback(this.list);
  }

  save(coffee, callback) {
    callback(true);
  }

  addCoffee(newCoffee) {
    this.list.push(newCoffee);
  }

}
